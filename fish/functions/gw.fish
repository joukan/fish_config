set rgubin_cfg $HOME/.gv.env

function gw
    set tf ""

	if test  "x$argv" != "x" 
		set tf (rgubin -f $rgubin_cfg -n $argv -t f)

        if test "x$tf" != "x" 
            nvim $tf
            return
        end

        set tfr (rgubin -f $rgubin_cfg -p -e $argv -t f | fzf --layout=reverse  --info=hidden -i --header="Selection:")
    else
        set tfr (rgubin -f $rgubin_cfg -p -t f -e "<Quit>" | fzf --layout=reverse  --info=hidden -i --header="Selection:")
    end

	set tf (echo $tfr | awk 'BETIN{FS=")"}{print $2}')
    if test "x$tf" = "x"; or test "x$tf" = "x<Quit>"
        return
    end

    nvim $tf
end

