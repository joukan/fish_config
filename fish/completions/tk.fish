set -l tk_commands add clean completions edit enqueue follow group kill log parallel pause remove reset restart send shutdown start  stash status switch wait

complete -c tk -f -n "not __fish_seen_subcommand_from $tk_commands" -a "$tk_commands"

#add
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--immediate" -d "Immediately start the task."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--print-task-id" -d "Only return the task id instead of a text."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--stashed" -d "Create the task in Stashed state."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--after" -d "Start after all specified tasks successfully exit."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--delay" -d "Enqueue task until <delay> elapses."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--group" -d "Assign the task to a group."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--label" -d "Add some custom information."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--working-directory" -d "Current working directory."
complete -c tk -f -n "__fish_seen_subcommand_from add" -a "--escape --help --version"


#status
complete -c tk -f -n "__fish_seen_subcommand_from status" -a "--json" -d "Print the current state as json to stdout."
complete -c tk -f -n "__fish_seen_subcommand_from status" -a "--group" -d "Only show tasks of a specific group."

#edit
complete -c tk -f -n "__fish_seen_subcommand_from edit" -a "--path" -d "Edit the path of the task."

#wait
complete -c tk -f -n "__fish_seen_subcommand_from wait" -a "--quiet" -d "Don't show any log output while waiting."
complete -c tk -f -n "__fish_seen_subcommand_from wait" -a "--group" -d "Wait for all tasks in a specific group."

#clean
complete -c tk -f -n "__fish_seen_subcommand_from clean" -a "--successful-only" -d "Only clean tasks that finished successfully."

#enqueue
complete -c tk -f -n "__fish_seen_subcommand_from enqueue" -a "--delay" -d "Delay enqueuing these tasks until <delay> elapses."

#log
complete -c tk -f -n "__fish_seen_subcommand_from log" -a "--full" -d "Show the whole stdout and stderr output."
complete -c tk -f -n "__fish_seen_subcommand_from log" -a "--json" -d "Print the resulting tasks and output as json."
complete -c tk -f -n "__fish_seen_subcommand_from log" -a "--lines" -d "Only print the last X lines of each task's output."

#reset
complete -c tk -f -n "__fish_seen_subcommand_from reset" -a "--force" -d "Don't ask for any confirmation."

#start
complete -c tk -f -n "__fish_seen_subcommand_from start" -a "--all" -d "Resume all groups."
complete -c tk -f -n "__fish_seen_subcommand_from start" -a "--children" -d "Also resume direct child processes of paused tasks."
complete -c tk -f -n "__fish_seen_subcommand_from start" -a "--group" -d "Resume a specific group."

#follow
complete -c tk -f -n "__fish_seen_subcommand_from follow" -a "--err" -d "Show stderr instead of stdout."

#parallel
complete -c tk -f -n "__fish_seen_subcommand_from parallel" -a "--group" -d "Set the amount for a specific group."

#restart
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--all-failed" -d "Restart all failed tasks accross all groups."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--edit" -d "Edit the tasks' command before restarting."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--in-place" -d "Restart the task by reusing the already existing tasks."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--start-immediately" -d "Immediately start the tasks."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--not-in-place" -d "Restart the task by creating a new identical tasks."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--edit-path" -d "Edit the tasks' path before restarting."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--stashed" -d "Set the restarted task to a "Stashed" state."
complete -c tk -f -n "__fish_seen_subcommand_from restart" -a "--failed-in-group" -d "Only restart tasks failed tasks of a specific group."

#group
complete -c tk -f -n "__fish_seen_subcommand_from group" -a "--add" -d "Add a group by name."
complete -c tk -f -n "__fish_seen_subcommand_from group" -a "--remove" -d "Remove a group by name."

#pause
complete -c tk -f -n "__fish_seen_subcommand_from pause" -a "--all" -d "Pause all groups."
complete -c tk -f -n "__fish_seen_subcommand_from pause" -a "--children" -d "Also pause direct child processes of a task."
complete -c tk -f -n "__fish_seen_subcommand_from pause" -a "--wait" -d "Only pause the specified group and let already running tasks finish."
complete -c tk -f -n "__fish_seen_subcommand_from pause" -a "--group" -d "Pause a specific group."

#kill
complete -c tk -f -n "__fish_seen_subcommand_from kill" -a "--all" -d "Kill all groups."
complete -c tk -f -n "__fish_seen_subcommand_from kill" -a "--children" -d "Send the SIGTERM signal to all children as well."
complete -c tk -f -n "__fish_seen_subcommand_from kill" -a "--signal" -d "Send a UNIX signal instead of simply killing the process."
complete -c tk -f -n "__fish_seen_subcommand_from pause" -a "--group" -d "Kill a specific group."
