if status is-interactive
    # Commands to run in interactive sessions can go here

    set LC_ALL      "en_US.UTF-8"
    set LANG        "en_US.UTF-8"
    set LANGUAGE    "en_US:en"

	set PATH ~/bin/aarch64-rpi3-linux-gnu/bin $PATH
	set PATH ~/bin $PATH
	set PATH ~/bin/docker-tools/bin $PATH
	set PATH /docker_bin/fzf/bin $PATH
    set PATH $HOME/.cargo/bin $PATH

    source ~/.aliases

    set CSCOPE_DB   "~/cstags"
    set JAVA_HOME   /usr/lib/jvm/java-8-oracle/bin/
    set PATH        ~/bin/go/bin $PATH
    set GOPATH      ~/go
    set GOBIN       ~/bin/go

    set X_TOOLS /home/joukan/x-tools
    set SSTATE_CACHE /home/joukan/work/sstate-cache
    set DOWNLOAD /home/joukan/work/downloads
    export DOCKER_PROPRIETARY X_TOOLS SSTATE_CACHE DOWNLOAD

    set INPUT_METHOD    ibus
    set GTK_IM_MODULE   ibus
    set XMODIFIERS      @im=ibus
    set QT_IM_MODULE    ibus

    #set XDG_DATA_DIRS $XDG_DATA_DIRS /home/joukan/.local/share/flatpak/exports/share
end

starship init fish | source
