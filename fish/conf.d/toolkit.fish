function reboot
    set result (string match -i -r '^\-\-help$' -- $argv | uniq)
    if test "x$result" != "x"
        /sbin/reboot --help
        return
    end

    set confirm (read -P "Are you sure to reboot? ")
    set result (string match -i -r '^(y|yes)$' $confirm | uniq)

    if test "x$result" != "x"
        /sbin/reboot $argv
    else
        echo "Reboot cancelled."
    end
end

function shutdown
    set result (string match -i -r '^\-\-help$' -- $argv | uniq)
    if test "x$result" != "x"
        /sbin/reboot --help
        return
    end

    set confirm (read -P "Are you sure to shutdown? ")
    set result (string match -i -r '^(y|yes)$' $confirm | uniq)

    if test "x$result" != "x"
        /sbin/shutdown $argv
    else
        echo "Shutdown cancelled."
    end
end
